import React from 'react';
import AdminApp from './AdminApp';
import { AdminLogin } from './AdminLogin';
import './AdminMain.css';

export class AdminMain extends React.Component {
    _adminAuthenticated = false;
    _email = "";
    _password = "";

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div className="admin-main-container">{this.getContent()}</div>
        );
    }

    getContent() {
        if (this._adminAuthenticated)
            return <AdminApp email={this._email} password={this._password} logout={this.logout.bind(this)}/>
        else 
            return <AdminLogin authenticatedCallback={this.authenticated.bind(this)}/>
    }

    authenticated (email: string, password: string) {
        this._email = email;
        this._password = password;
        this._adminAuthenticated = true;
        this.forceUpdate();
    }

    logout() {
        this._email = "";
        this._password = "";
        this._adminAuthenticated = false;
        this.forceUpdate();
    }
}