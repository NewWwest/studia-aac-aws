import React from 'react';
import { ApiProxy } from '../services/ApiProxy';
import { FakeApi } from '../services/FakeApi';
import './AdminApp.css'
interface AdminInterface {
  email: string,
  password: string,
  logout: () => void
}

export class AdminApp extends React.Component<AdminInterface> {
  _words: string[] = [];
  _loading: boolean = false;
  _api = new ApiProxy();

  constructor(props: AdminInterface) {
    super(props);

    try {
      let temp = (this as any).props.match.params.word;
      if (temp != undefined && temp != '') {
        this._words = [temp]
      }
    }
    catch (e) { }
    if (this._words == undefined || this._words.length == 0) {
      this._loading = true;
      this._api.getWordSuggestions(this.props.email, this.props.password).then(words => {
        this._loading = false;
        this._words = words;
        this.forceUpdate();
      });
    }
  }

  render() {
    if (this._loading) {
      return (<div>loading...</div>);
    }
    else {
      return (
        <div>
          <h2 className="admin-words-table-header">Do you want to add this word to dictionary?</h2>
          <table className="words-table">
            <tbody>
              {
                this._words.map((value: string) => { return this.renderWord(value) })
              }
            </tbody>
          </table>
          <div>
            <button className="main-btn" onClick={this.props.logout}>Logout</button>
          </div>
        </div>
      );
    }
  }

  renderWord(word: string) {
    return (
      <tr>
        <td className="words-column"><p>{word}</p></td>
        <td><button className="delete-btn base-btn word-btn" onClick={this.DeleteWord.bind(this, word)}>No, delete this suggestion</button></td>
        <td><button className="accept-btn base-btn word-btn" onClick={this.AddWord.bind(this, word)}>Yes, add this to game</button></td>
      </tr>
    )
  }

  DeleteWord(word: string) {
    this._api.deleteWordSuggestion(word, this.props.email, this.props.password).then(() => {
      this._words = this._words.filter((value) => value != word);
      this.forceUpdate();
    });
  }

  AddWord(word: string) {
    this._api.deleteWordSuggestion(word, this.props.email, this.props.password).then(() => {
      this._words = this._words.filter((value) => value != word);
      this.forceUpdate();
    });
    this._api.addWord(word, this.props.email, this.props.password).then(() => { });
  }
}

export default AdminApp;