import React from "react";
import '../../shared/shared.css'

interface InfoProperties { onBeginFunction: ()=>void }

export class Info extends React.Component<InfoProperties> {

    constructor(props: Readonly<InfoProperties>) {
        super(props);
    }

    startGame(event: React.MouseEvent<HTMLButtonElement, MouseEvent>){
        this.props.onBeginFunction();
    }

    render() {
        return (
            <div className="info">
                <p className="info-part">Hello there!</p>
                <p className="info-part">When you press the button below random letters will be shown to you. <br/>
                    Your job is to create as many words as possible using only them. <br/>
                    If you make a mistake the word won't be counted</p>
                <p className="info-part">Good luck</p>
                <button className="main-btn" onClick={this.startGame.bind(this)}>Start</button>
            </div>
        );
    }

}