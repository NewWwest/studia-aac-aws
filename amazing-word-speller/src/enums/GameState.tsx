export enum GameState{
    Awaiting,
    InProgress,
    Finished
}