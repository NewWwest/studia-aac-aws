const AWS = require('aws-sdk');
const dynamoClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//Model:
//{
//  "words":[ "Ford", "BMW", "Fiat" ]
//}
exports.handler = function (event, context, callback) {

    console.log('Received event:', JSON.stringify(event, null, 2));
    const WordsTable = process.env.WordsTable;
    var words = JSON.parse(event.body).words;

    if (words === undefined) {
        callback("400 Invalid Input");
    }

    var keys = []
    for (var i = 0; i < words.length; i++) {
        keys[i] = {
            "word": words[i]
        };
    }

    var params = {RequestItems: {}}
    params.RequestItems[WordsTable]={Keys: keys};

    dynamoClient.batchGet(params, function (err, data) {
        if (err) {
            console.log(JSON.stringify(err));
            callback(err, null);
        } else {
            console.log('Retrieved data:', data); //{ Responses: { Words: [ [Object], [Object] ] },
            var correctWordsRetrieved = data.Responses[WordsTable];

            var correctWordsToReturn = [];
            for (var i = 0; i < correctWordsRetrieved.length; i++) {
                console.log(correctWordsRetrieved[i]); //{ word: 'a' }
                correctWordsToReturn[i] = correctWordsRetrieved[i].word;
            };
            var response = {
                "statusCode": 200,
                "body": JSON.stringify(correctWordsToReturn),
                "isBase64Encoded": false,
				headers: {
					"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Headers": "*"
				},
            };
            callback(null, response);
        }
    });
};