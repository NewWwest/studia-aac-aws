const AWS = require('aws-sdk');
const crypto = require('crypto');
const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

exports.handler = function (event, context, callback) {
	var body = JSON.parse(event.body);
	const email = body.email;
	const password = body.password;
	console.log("Veryfing auth for email: "+email)
	
    if (email === undefined || password === undefined) {
        var response = {
            "statusCode": 401,
			headers: {
				"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Allow-Headers": "*"
			},
        };
        callback(null, response);
    }

	var passwordHash = crypto.createHmac('sha256', password).digest('hex');
	
    var params = {
	    TableName: process.env.AdminsTable,
	    Key:{
	        "email": email
	    }
	};
	
	docClient.get(params, function(err, data) {
	    if (err) {
	        callback(err, null);
	    } else {
	        if (data.Item != undefined && data.Item.password == passwordHash) {
	            var response = {
                    "statusCode": 200,
    				headers: {
    					"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
    					"Access-Control-Allow-Origin": "*",
    					"Access-Control-Allow-Headers": "*"
    				},
                };
	            callback(null, response);
	        }
	        else {
	            var response = {
                    "statusCode": 401,
    				headers: {
    					"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
    					"Access-Control-Allow-Origin": "*",
    					"Access-Control-Allow-Headers": "*"
    				},
				};

				if(data.Item == undefined){
					console.log("No user")
				}else{
					console.log("Wrong Password")
				}
				
	            callback(null, response);
	        }
	    }
	});
};