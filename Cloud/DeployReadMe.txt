#Looks at code in current working directory, builds it, packages, uplpoads to S3, packaged.yaml has urls to created packages on S3
sam package --s3-bucket cloud-formation-deployments-2016mini --template-file template.yaml --output-template-file packaged.yaml

#deploy modify the EnvironmentAppendix and stack name parameter as you wish
sam deploy --template-file packaged.yaml `
--stack-name prod `
--capabilities CAPABILITY_NAMED_IAM `
--parameter-overrides "EnvironmentAppendix=prod" 
